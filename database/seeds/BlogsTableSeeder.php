<?php

use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blogs')->insert([
            [
                'title' => 'test1',
                'content' => 'Dit is een test blog.',
                'user_id' => '1',
                'created_at' => now()
            ],
            [
                'title' => 'test2',
                'content' => 'Dit is een 2de test blog.',
                'user_id' => '2',
                'created_at' => now()
            ]
        ]);
    }
}
