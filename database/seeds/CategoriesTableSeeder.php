<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'test',
                'created_at' => now()
            ],
            [
                'name' => 'Laravel',
                'created_at' => now()
            ]
        ]);
    }
}
