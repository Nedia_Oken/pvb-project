<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Danny',
                'email' => 'danny@gmail.com',
                'address' => 'Dannystreet 56',
                'postalcode' => '1234GE',
                'place' => 'Groningen',
                'phonenumber' => '0503092746',
                'profilepicture' => 'default.png',
                'role_id' => '1',
                'password' => bcrypt('Danny123'),
                'created_at' => now()
            ],
            [
                'name' => 'Patrick',
                'email' => 'patrick@customwebsite.nl',
                'address' => 'Patrickstreet 21',
                'postalcode' => '4321CH',
                'place' => 'Groningen',
                'phonenumber' => '0503098926',
                'profilepicture' => 'default.png',
                'role_id' => '1',
                'password' => bcrypt('Patrick123'),
                'created_at' => now()
            ],
            [
                'name' => 'Jochem',
                'email' => 'jochemwachenmakers@gmail.com',
                'address' => 'Jochemstreet 42',
                'postalcode' => '5678JK',
                'place' => 'Groningen',
                'phonenumber' => '0503094756',
                'profilepicture' => 'default.png',
                'role_id' => '1',
                'password' => bcrypt('Jochem123'),
                'created_at' => now()
            ],
            [
                'name' => 'George',
                'email' => 'georgeanti99@gmail.com',
                'address' => 'Kluivingskampenweg 78',
                'postalcode' => '9761BR',
                'place' => 'Eelde',
                'phonenumber' => '0503094806',
                'profilepicture' => 'default.png',
                'role_id' => '1',
                'password' => bcrypt('George123'),
                'created_at' => now()
            ]
        ]);
    }
}
